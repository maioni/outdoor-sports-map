require 'chunky_png'
require 'rmagick'
require 'chunky_png/rmagick'
class PixelTile
  attr_accessor :x
  attr_accessor :y
  attr_accessor :types
  def initialize x,y,type
    @x=x
    @y=y
    @types=Array.new
    @types << type
  end
  def add_type type
    @types << type
  end
  def include_type? type
    @types.include?(type)
  end
end


class TileProcess

  attr_accessor :data
  attr_accessor :adj_hash
  attr_accessor :pixels
  attr_accessor :inc_opacity

  def initialize data, ntypes
    @data = data
    @adj_hash = Hash.new(0)
    @pixels=Hash.new()
    if(ntypes>0)
      @inc_opacity= 80/ntypes
    else
      @inc_opacity=0
    end
  end

  def process
    # process  sensors vector and construct a image of intersection of them  
    size = 513
    latlongi=[-23,-47.024]
    # ----------------------------------------------------------------
    png = ChunkyPNG::Image.new(size,size, ChunkyPNG::Color::TRANSPARENT)
    for x in 0...size
      for y in 0...size
        png[x,y] = ChunkyPNG::Color.from_hsl(0,0,0,0)
        @pixels[(x.to_s+','+y.to_s)]=PixelTile.new(x,y,'')
      end
    end
    # ----------------------------------------------------------------
    sensors=@data
    npoints=sensors.length
    i = 0;
    npoints.times do
      pxy=geographic_to_coordinates([(sensors[i].latitude).to_f,(sensors[i].longitude).to_f],latlongi)
      r=kilometres_to_pixel((sensors[i].radius).to_f)
      mh=(sensors[i].color[0]).to_i
      mtype=sensors[i].type
      if not @adj_hash.has_key?(r)
        create_adjacency(r)
      end
      adj_arr = @adj_hash[r]
      adj_dx = adj_arr[0]
      adj_dy = adj_arr[1]
      adj_dx.zip(adj_dy).each do
        |adx,ady|
        nx = pxy[0] + adx
        ny = pxy[1] + ady
        if ( nx >= 0 and nx < size and ny >= 0 and ny < size)
          value = ChunkyPNG::Color.to_hsl(png[nx,ny],true)
          newType=@pixels[(nx.to_s+','+ny.to_s)].include_type?(mtype)
          if value[3] != 0
            alpha = 255
            if(!newType)
              @pixels[(nx.to_s+','+ny.to_s)].add_type(mtype)
              alpha= value[3]+@inc_opacity
            else
              alpha=value[3]
            end
            if alpha>160 
              alpha=160 
            end
             png[nx,ny] = ChunkyPNG::Color.from_hsl((mh+value[0])/2,0.9,0.5,alpha.to_i)
          else  
            if @inc_opacity==80
              png[nx,ny] = ChunkyPNG::Color.from_hsl(mh,0.9,0.5,160)
            else
              png[nx,ny] = ChunkyPNG::Color.from_hsl(mh,0.9,0.5,80)
            end
            @pixels[(nx.to_s+','+ny.to_s)].add_type(mtype)
          end
        end
      end
      i = i + 1
    end
    img = ChunkyPNG::RMagick.export(png)
    img = img.gaussian_blur(0, 2)
    nome = aleatory_string(20)+".png"  
    #---------------------------------------------------------------
    # TODO ##
    # Manipulate with Rails.root pending for failed into tests
    # string_file = Rails.root.join('app', 'assets', 'images', nome)    
    # img.write (string_file)
    #---------------------------------------------------------------
    # string_file = "./app/assets/images/process/"+nome
     string_file = "./public/images/process/"+nome
    img.write (string_file)
    return nome
  end
  
  def geographic_to_coordinates latlong, latlongi
    # calculate a location of geographic coordinates to coordinates in the image
    x=degrees_to_pixel(latlong[1]-latlongi[1])
    y=degrees_to_pixel(-(latlong[0]-latlongi[0]))
    [x,y]
  end
  
  def kilometres_to_pixel k
    # convert kilometres to pixels
    (k * 5).to_i
  end
  
  def degrees_to_pixel d
    # convert degrees to pixels
    (d * 513 / 1.024).to_i 
  end
  
  def create_adjacency r
    # create a adjacent pixel hash for all radius 
    r2 = r*r
    adj_dx = [];
    adj_dy = [];
    for dx in -r..r
      for dy in -r..r
        if(((dx*dx)+(dy*dy)) <= r2)
          adj_dx << dx
          adj_dy << dy
        end
      end
    end
    @adj_hash[r] = [adj_dx , adj_dy]
  end
  
  def aleatory_string n 
  # create a aleatory string
    o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten    
    return (0...n).map { o[rand(o.length)] }.join
  end
  
end

