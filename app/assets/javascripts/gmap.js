var map;
var mylocation=null;
function initMap() {
  // Create a map object and specify the DOM element for display and verify if exists a image for display into the map
  map = new google.maps.Map(document.getElementById('map-canvas'), {
    center: {lat: -23.558745, lng: -46.731859},
    scrollwheel: true,
    zoom: 12,
    mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      position: google.maps.ControlPosition.TOP_CENTER,
    }
  });
  
  if(image){
    var el = document.getElementById("legend");
    el.style.display="block";
    map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(document.getElementById('legend'));
  }
  
  getValues();
  if (!image.length == 0) {
  setImage();
  }
  if(op5!=""){
    setGreenAreas();
  }
  if(op6!=""){
    setBikePaths();
  }
};
function setBikePaths() {
  // Show bike paths
  var bikeLayer = new google.maps.BicyclingLayer();
  bikeLayer.setMap(map); 
}
function setGreenAreas(){
  //Show green areas
  var json = JSON.parse(gon.json)["data"];
 
  for(var i=0;i<json.length;i++){
    var m = json[i];
    var marker = new google.maps.Marker({
      position: {lat: parseFloat(m["latitude"]), lng: parseFloat(m["longitude"])},
      map: map,
      title: 'test',
      icon: 'https://cdn1.iconfinder.com/data/icons/professional-toolbar-icons-png/32/Run.png'
    });  
  }
}

function setImage(){
  // show image
  var img = new Image();
     var img_url = './images/process/'+image
    // var img_url = "../public/process/"+image
    
    img.src= img_url;
    var imageBounds = {
      north: -23,
      south: -24.024,
      east: -46,
      west: -47.024
    };
  var layer1 = new google.maps.GroundOverlay(img_url,imageBounds);
  layer1.setMap(map);
 
}

function showPosition(position) {
  // show localization in the map
  $("#latitude").val(position.coords.latitude);
  $("#longitude").val(position.coords.longitude);
  var lat=position.coords.latitude;
  var lon=position.coords.longitude;
  var latlon=new google.maps.LatLng(lat, lon)
  var mapholder=document.getElementById('map-canvas')
	
  var myOptions={
    center:latlon,zoom:12,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    mapTypeControl:false,
    navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
  };
  //var map=new google.maps.Map(document.getElementById("map-canvas"),myOptions);
  if(mylocation!=null) clearMarkers()
  mylocation=new google.maps.Marker({position:latlon,map:map,title:"You are here"});
	
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode({ 'latLng': mylocation.getPosition() }, function (results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[0]) { 
        $('#localization').val(results[0].formatted_address);
      }
    }
  });
}

function getLocalization() {
  if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition);
  }
  else{
    $("#localization").val("Geolocation is not supported by this browser.");
  }
}

function search(){
// search a localization and show in the map
  var geocoder = new google.maps.Geocoder();
	geocoder.geocode({ 'address': $("#localization").val() + ', Brasil', 'region': 'BR' }, function (results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[0]) {
        var latitude = results[0].geometry.location.lat();
        var longitude = results[0].geometry.location.lng();
				
        $('#localization').val(results[0].formatted_address);
        $('#latitude').val(latitude);
        $('#longitude').val(longitude);
				
        var location = new google.maps.LatLng(latitude, longitude);
        if(mylocation!=null) clearMarkers()
        mylocation=new google.maps.Marker({position:location,map:map,title:"You are here"});
        
        map.setCenter(location);
        map.setZoom(12);
     }
    } 
  });
}
function clearMarkers() {
  // clear markers on the map
  mylocation.setMap(null);
}
function onlyNumber(e){
  key=(window.event)?event.keyCode:e.which;
  return ((key>47 && key<58) || key==8 || key==0);
}

function getValues() {
  // fills fields with values
   $('#localization').val(localizationp);
   $('#latitude').val(latitudep);
   $('#longitude').val(longitudep);
   $('#radius').val(radiusp);
   if(op1!="") document.getElementById("air-quality").checked = true;
   if(op2!="") document.getElementById("ultraviolet").checked = true;
   if(op3!="") document.getElementById("humidity").checked = true;
   if(op4!="") document.getElementById("temperature").checked = true;
   if(op5!="") document.getElementById("green-areas").checked = true;
   if(op6!="") document.getElementById("bike-paths").checked = true;
   if(latitudep !="" && longitudep !="")
     search()
   return;
}

window.onbeforeunload = function (){
 $.ajax({
    type:'GET',
    async: false,
    url:'/deleteFile',
    dataType: "json",
    data: { "image" : image
          },
    success:function(){
      console.debug("teste");
    }, error: function(xhr){
      console.debug(xhr);
    }
  });
};