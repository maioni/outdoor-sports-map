require_relative 'tile_process.rb'
require_relative 'sensor.rb'
require 'json'
class DataProcess

  attr_accessor :sensors
  def initialize
    @sensors= Array.new 
    @ntypes=0
  end
  
  def discovery(key, address, params)
    rad = (1000*params[:sports_map][:radius].to_i).to_s
    url_param = "/discovery/resources?capability=" + key.downcase + "&lat=" + params[:sports_map][:latitude] + "&lon=" + params[:sports_map][:longitude] + "&radius=" + rad
    Net::HTTP.get_response(address, url_param, 3004)
  end
   
  def get_all_sensors(params)
    data = Array.new
     
    checked_parameters = Hash.new
    checked_parameters["Pollution"] = params[:sports_map][:options]['1']
    checked_parameters["UV"] = params[:sports_map][:options]['2']
    checked_parameters["Humidity"] = params[:sports_map][:options]['3']
    checked_parameters["Temperature"] = params[:sports_map][:options]['4']
 
    #address = "107.170.158.70"
    address = "cpro37690.publiccloud.com.br"
    
    coords = discovery("temperature", address, params); #all capacities have the same coordinates, so we simply chose temperature
                   
    uuids = uuids(coords.body)

    checked_parameters.each do |key, value|
      if value
        data.concat(data_to_input_file(key, coords.body));
        @ntypes=@ntypes+1
      end
    end

    #query with group 3, but not used
    response = RestClient.post "http://107.170.158.70:3002/resources/data/last", {"sensor_value" => {"uuids" => uuids}}.to_json, content_type:"application/json"

    hash_in = Hash.new
    hash_in["data"] = data
    json_in = JSON.generate(hash_in)
    json_in
  end
   
  def uuids(coords_json)
    uuids = Array.new
    coords = JSON.parse(coords_json)["resources"]
    coords.each do |c|
      uuids << c["uuid"]
    end
    uuids
  end
   
  def data_to_input_file(capability, coords_json)
    coords = JSON.parse(coords_json)["resources"]
    coords.each do |c| 
      c.delete "collect_interval"
      c["id"] = c.delete "uuid"
      str = ""
      case capability
      when "Temperature"
        str = "temperature"
        c["value"] = Random.rand -20...36
      when "Humidity"
        str = "humidity"
        c["value"] = Random.rand 5...100
      when "Pollution"
        str = "airquality"
        c["value"] = Random.rand 0...200
      when "UV"
        str = "uvindex"
        c["value"] = Random.rand 0...15
      end
      c["type"] = str
      c["radius"] = 2.to_s
      c["latitude"] = c["lat"].to_s
      c.delete "lat"
      c["longitude"] = c["lon"].to_s
      c.delete "lon"
    end
    coords
  end
  
  def loadSensor(params)
    # Here the query  for sensors is processed , that are within radius for consultation.
    #only consider parameters[:options]['1'],parameters[:options]['2'],parameters[:options]['3'],parameters[:options]['4'] for type of sensors to load 
    #  parameters[:radius], parameters[:latitude], parameters[:longitude]
    if params != nil
      jsensors=JSON.parse(get_all_sensors(params))
      tabBounds=JSON.parse(File.read("app/assets/dataInput/boundSensor"))
      jsensors['data'].each do
        |s|
        o=DrawableSensor.new(s["id"],s["value"],s["latitude"],s["longitude"],s["type"],s["radius"],1)
        o.setColor tabBounds
        @sensors << o
      end
    
    else
      jsensors = nil
    end
    jsensors
  end


  def processSensors
    # Processes the intersection of sensors and gets the points to draw on the map
    image_tile = TileProcess.new(@sensors,@ntypes)
    image_tile.process
  end
end